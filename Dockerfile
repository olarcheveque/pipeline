FROM heroku/heroku:18-build

WORKDIR /app

RUN apt-get update && apt-get install -y python-pip && rm -rf /var/lib/apt/lists/*

ENV PYTHONPATH ${PYTHONPATH}:/app/

COPY requirements.txt /app/
COPY requirements /app/requirements

RUN pip install --disable-pip-version-check gunicorn==19.7.1
RUN pip install --disable-pip-version-check --no-cache-dir -r /app/requirements.txt

COPY . /app
CMD gunicorn config.wsgi:application
